program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Locadora},
  Unit2 in 'Unit2.pas' {CadastrarCliente},
  Unit3 in 'Unit3.pas' {AdicionarFilme};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TLocadora, Locadora);
  Application.CreateForm(TCadastrarCliente, CadastrarCliente);
  Application.CreateForm(TAdicionarFilme, AdicionarFilme);
  Application.Run;
end.
