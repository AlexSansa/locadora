object AdicionarFilme: TAdicionarFilme
  Left = 377
  Top = 192
  Width = 522
  Height = 331
  Caption = 'AdicionarFilme'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Nome: TLabel
    Left = 160
    Top = 40
    Width = 28
    Height = 13
    Caption = 'Nome'
  end
  object Genero: TLabel
    Left = 160
    Top = 80
    Width = 35
    Height = 13
    Caption = 'G'#234'nero'
  end
  object Quantidade: TLabel
    Left = 152
    Top = 128
    Width = 55
    Height = 13
    Caption = 'Quantidade'
  end
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 137
    Height = 153
    TabOrder = 5
    object capa: TImage
      Left = 16
      Top = 8
      Width = 105
      Height = 105
      Stretch = True
    end
  end
  object Edit1: TEdit
    Left = 216
    Top = 40
    Width = 233
    Height = 21
    TabOrder = 0
  end
  object ComboBox1: TComboBox
    Left = 216
    Top = 80
    Width = 145
    Height = 21
    ItemHeight = 13
    TabOrder = 1
    Items.Strings = (
      'A'#231#227'o'
      'Anima'#231#227'o'
      'Aventura'
      'Com'#233'dia'
      'Drama'
      'Romance'
      'Suspense'
      'Terror')
  end
  object Edit2: TEdit
    Left = 216
    Top = 120
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object Pesquisar: TButton
    Left = 32
    Top = 128
    Width = 91
    Height = 25
    Caption = 'Pesquisar Imagem'
    TabOrder = 3
    OnClick = PesquisarClick
  end
  object Salvar: TButton
    Left = 184
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 4
  end
  object opd: TOpenPictureDialog
    Left = 8
    Top = 184
  end
end
