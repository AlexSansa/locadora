unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;

type
  TLocadora = class(TForm)
    Buscar: TButton;
    Edit1: TEdit;
    Adicionar: TButton;
    Reservar: TButton;
    Cadastrar_Cliente: TButton;
    Exibir_Todos: TButton;
    DBGrid1: TDBGrid;
    procedure BuscarClick(Sender: TObject);
    procedure Cadastrar_ClienteClick(Sender: TObject);
    procedure AdicionarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Locadora: TLocadora;

implementation

uses Unit3, Unit2;

{$R *.dfm}

procedure TLocadora.BuscarClick(Sender: TObject);
var buscar : String;
begin
        buscar :=Edit1.Text;

        if buscar ='' then
          ShowMessage('Necessário preencher o campo');
end;

procedure TLocadora.Cadastrar_ClienteClick(Sender: TObject);
begin
CadastrarCliente.ShowModal;
end;

procedure TLocadora.AdicionarClick(Sender: TObject);
begin
AdicionarFilme.ShowModal;
end;

end.
