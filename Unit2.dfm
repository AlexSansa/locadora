object CadastrarCliente: TCadastrarCliente
  Left = 195
  Top = 224
  Width = 519
  Height = 205
  Caption = 'Cadastrar Cliente'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Nome: TLabel
    Left = 72
    Top = 16
    Width = 28
    Height = 13
    Caption = 'Nome'
  end
  object CPF: TLabel
    Left = 72
    Top = 80
    Width = 20
    Height = 13
    Caption = 'CPF'
  end
  object endereco: TLabel
    Left = 72
    Top = 48
    Width = 46
    Height = 13
    Caption = 'Endere'#231'o'
  end
  object Edit1: TEdit
    Left = 136
    Top = 8
    Width = 225
    Height = 21
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 136
    Top = 72
    Width = 225
    Height = 21
    TabOrder = 1
  end
  object Salvar: TButton
    Left = 136
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Salvar'
    TabOrder = 2
  end
  object Edit3: TEdit
    Left = 136
    Top = 40
    Width = 225
    Height = 21
    TabOrder = 3
  end
end
