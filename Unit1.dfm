object Locadora: TLocadora
  Left = 192
  Top = 120
  Width = 519
  Height = 300
  Caption = 'Locadora'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Buscar: TButton
    Left = 408
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Buscar'
    TabOrder = 0
    OnClick = BuscarClick
  end
  object Edit1: TEdit
    Left = 40
    Top = 32
    Width = 345
    Height = 21
    TabOrder = 1
  end
  object DBGrid1: TDBGrid
    Left = 40
    Top = 64
    Width = 345
    Height = 120
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object Adicionar: TButton
    Left = 408
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Adicionar'
    TabOrder = 3
    OnClick = AdicionarClick
  end
  object Reservar: TButton
    Left = 408
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Reservar'
    TabOrder = 4
  end
  object Cadastrar_Cliente: TButton
    Left = 80
    Top = 200
    Width = 105
    Height = 25
    Caption = 'Cadastrar Cliente'
    TabOrder = 5
    OnClick = Cadastrar_ClienteClick
  end
  object Exibir_Todos: TButton
    Left = 272
    Top = 200
    Width = 75
    Height = 25
    Caption = 'Exibir Todos'
    TabOrder = 6
  end
end
